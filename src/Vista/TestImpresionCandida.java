/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Fraccion;
import Modelo.MatrizEntero;
import Modelo.Persona;
import Util.Dato;

/**
 *
 * @author docenteauditorio
 */
public class TestImpresionCandida {

    public static void main(String[] args) {
        String nombre = "Diana M";
        String nombre2 = "diana M";
        int r = 6, r2 = 4;
        float x = 0.9F;
        Fraccion f = new Fraccion(3, 4);
        Fraccion f2=new Fraccion(3,2);
        
        Persona p1=new Persona(1,1982,(byte)4,(byte)5,(byte)2,(byte)3);
        Persona p2=new Persona(1,1983,(byte)4,(byte)5,(byte)2,(byte)3);

        MatrizEntero m = new MatrizEntero(3, 3);
        MatrizEntero m2 = new MatrizEntero(3, 3);
//        System.out.println(nombre);
//        System.out.println(r);
//        System.out.println(x);
//        System.out.println(f);

        Dato<String> d1 = new Dato(nombre);
        Dato<String> d12 = new Dato(nombre2);
        Dato<Fraccion> d2 = new Dato(f);
        //La parametrización se da a nivel de objetos

        Dato<Integer> d3 = new Dato(r);
        Dato<Integer> d32 = new Dato(r2);

        Dato<Float> d4 = new Dato(x);

        Dato<MatrizEntero> d5 = new Dato(m);

        d1.imprimirPorConsola();
        d2.imprimirPorConsola();
        d3.imprimirPorConsola();
        d4.imprimirPorConsola();
        d5.imprimirPorConsola();

        System.out.println("Mirando menores a través de compareTo");
        //Contenedor vs tipo de dato
        System.out.println("el menor de los enteros es:" + d3.getMenor(r2));
        System.out.println("el menor de strigs:"+d1.getMenor(nombre2));
        System.out.println("el menor de los enteros es:" + d3.getMenor(r2));
        System.out.println("el menor de fracciones:"+d2.getMenor(f2));
        //Contenedor vs Contendor
        System.out.println("el menor de los enteros es:" + d3.getMenor2(d32));
        System.out.println("el menor de strigs:"+d1.getMenor2(d12));
    }
}
